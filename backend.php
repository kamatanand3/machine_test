<?php 
session_start();
if (isset($_POST['submit'])) {
	if (empty($_POST['uid'])) {
		
		$education_arr = array();
		$education = array();
		$passing_year_arr = array();
		$skill_arr = array();
		$certificates_arr = array();
		$photo =time() . '-' .$_FILES['profileImage']['name'];
		$target_dir = "images/";
		$target_file = $target_dir . basename($photo);
		move_uploaded_file($_FILES["profileImage"]["tmp_name"], $target_file);
		$certificates_arr = $_FILES['files']['name'];
		foreach ($_FILES["files"]["tmp_name"] as $key=>$tmp_name) {
			$file_name=$_FILES["files"]["name"][$key];
			move_uploaded_file($file_tmp=$_FILES["files"]["tmp_name"][$key],"certificates/".time().$file_name);
		}
		$certificates = implode(", ", $certificates_arr);
		$name = stripslashes($_POST['name']);
		$birthday = stripslashes($_POST['dob']);
		$gender = stripslashes($_POST['gender']);
		$state = stripslashes($_POST['state']);
		$city = stripslashes($_POST['city']);
		$education_arr= $_POST['education'];
		$education = implode(", ",$education_arr);
		$passing_year_arr =$_POST['year_Of_completion'];
		$passing_year = implode(", ",$passing_year_arr);
		$skill_arr = $_POST['skills'];
		$skill = implode(", ", $skill_arr);
		$profession = $_POST['profession'];
		$company_name = stripslashes($_POST['cname']);
		$date_of_joining = stripslashes($_POST['doj']);
		$business_name = stripslashes($_POST['bname']);
		$location = stripslashes($_POST['location']);
		$email_id = stripslashes($_POST['email_id']);
		$mobile_no = stripslashes($_POST['mobile_no']);
		$created_at = date("Y-m-d h:i:s");

		$data1 = array (
			"photo" => $photo,
			"certificates" => $certificates,
			"name" => $name,
			"birthday" => $birthday,
			"gender" => $gender,
			"state" => $state,
			"city" => $city,
			"education" =>$education,
			"passing_year" =>$passing_year,
			"skill" =>$skill,
			"profession" => $profession,
			"company_name" =>$company_name,
			"date_of_joining" => $date_of_joining,
			"business_name" => $business_name,
			"location" =>$location,
			"email_id" =>$email_id,
			"mobile_no" =>$mobile_no,
			'created_at' =>$created_at 
		);
	// echo"<pre>";print_r($data1);
		$url ="http://localhost:8080/machine_test/api/details/create.php";
		$ch = curl_init ( $url );
		curl_setopt ( $ch, CURLOPT_HTTPHEADER, array (
			'Content-Type: application/json' 
		) );

		$data1 = json_encode ( $data1 );
		curl_setopt ( $ch, CURLOPT_URL, $url );
		curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
		curl_setopt ( $ch, CURLOPT_POST, true );
		curl_setopt ( $ch, CURLOPT_POSTFIELDS, $data1 );
		curl_setopt ( $ch, CURLOPT_SSL_VERIFYPEER, false );
		$responseonpost = curl_exec ( $ch );
		curl_close ( $ch );
		$data = $responseonpost;
		$data = json_decode( $data, true );
		if (!empty($data)) {
			$_SESSION['msg']='New Recode has been added.';
			header('Location: add_new.php');
		}
	}else{

		$education_arr = array();
		$education = array();
		$passing_year_arr = array();
		$skill_arr = array();
		$certificates_arr = array();
		$photo =time() . '-' .$_FILES['profileImage']['name'];
		$target_dir = "images/";
		$target_file = $target_dir . basename($photo);
		move_uploaded_file($_FILES["profileImage"]["tmp_name"], $target_file);
		$certificates_arr = $_FILES['files']['name'];
		foreach ($_FILES["files"]["tmp_name"] as $key=>$tmp_name) {
			$file_name=$_FILES["files"]["name"][$key];
			move_uploaded_file($file_tmp=$_FILES["files"]["tmp_name"][$key],"certificates/".time().$file_name);
		}
		$certificates = implode(", ", $certificates_arr);
		$id = stripslashes($_POST['uid']);
		$name = stripslashes($_POST['name']);
		$birthday = stripslashes($_POST['dob']);
		$gender = stripslashes($_POST['gender']);
		$state = stripslashes($_POST['state']);
		$city = stripslashes($_POST['city']);
		$education_arr= $_POST['education'];
		$education = implode(", ",$education_arr);
		$passing_year_arr =$_POST['year_Of_completion'];
		$passing_year = implode(", ",$passing_year_arr);
		$skill_arr = $_POST['skills'];
		$skill = implode(", ", $skill_arr);
		$profession = $_POST['profession'];
		$company_name = stripslashes($_POST['cname']);
		$date_of_joining = stripslashes($_POST['doj']);
		$business_name = stripslashes($_POST['bname']);
		$location = stripslashes($_POST['location']);
		$email_id = stripslashes($_POST['email_id']);
		$mobile_no = stripslashes($_POST['mobile_no']);
		$created_at = date("Y-m-d h:i:s");

		$data1 = array (
			"id" => $id,
			"photo" => $photo,
			"certificates" => $certificates,
			"name" => $name,
			"birthday" => $birthday,
			"gender" => $gender,
			"state" => $state,
			"city" => $city,
			"education" =>$education,
			"passing_year" =>$passing_year,
			"skill" =>$skill,
			"profession" => $profession,
			"company_name" =>$company_name,
			"date_of_joining" => $date_of_joining,
			"business_name" => $business_name,
			"location" =>$location,
			"email_id" =>$email_id,
			"mobile_no" =>$mobile_no,
			'created_at' =>$created_at 
		);
	// echo"<pre>";print_r($data1);
		$url ="http://localhost:8080/machine_test/api/details/delete.php?id=1";
		$ch = curl_init ( $url );
		curl_setopt ( $ch, CURLOPT_HTTPHEADER, array (
			'Content-Type: application/json' 
		) );

		$data1 = json_encode ( $data1 );
		curl_setopt ( $ch, CURLOPT_URL, $url );
		curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
		curl_setopt ( $ch, CURLOPT_POST, true );
		curl_setopt ( $ch, CURLOPT_POSTFIELDS, $data1 );
		curl_setopt ( $ch, CURLOPT_SSL_VERIFYPEER, false );
		$responseonpost = curl_exec ( $ch );
		curl_close ( $ch );
		$data = $responseonpost;
		$data = json_decode( $data, true );
		if (!empty($data)) {
			$_SESSION['msg']='Recode has been Updated.';
			header('Location: add_new.php?id='.$id);
		}

	}

	// print_r($data);
}

if (isset($_GET[id])) {
	$id = $_GET[id];

	$data1 = array (
		"id" => $id
	);
	// echo"<pre>";print_r($data1);
	// exit();
	$url ="http://localhost:8080/machine_test/api/details/delete.php?id=".$id;
	$ch = curl_init ( $url );
	curl_setopt ( $ch, CURLOPT_HTTPHEADER, array (
		'Content-Type: application/json' 
	) );

	$data1 = json_encode ( $data1 );
	curl_setopt ( $ch, CURLOPT_URL, $url );
	curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
	curl_setopt ( $ch, CURLOPT_POST, true );
	curl_setopt ( $ch, CURLOPT_POSTFIELDS, $data1 );
	curl_setopt ( $ch, CURLOPT_SSL_VERIFYPEER, false );
	$responseonpost = curl_exec ( $ch );
	curl_close ( $ch );
	$data = $responseonpost;
	$data = json_decode( $data, true );
	if (!empty($data)) {
		$_SESSION['msg']='Recode has been Updated.';
		header('Location: user_list.php');
	}
}


?>