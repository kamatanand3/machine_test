<?php 
session_start();

if ($_GET[id]) {
	$url="http://localhost:8080/machine_test/api/details/read_one.php?id=".$_GET[id];
	$ch=curl_init();
	curl_setopt($ch,CURLOPT_URL,$url);
	curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
	curl_setopt($ch, CURLOPT_HEADER, false );
	curl_setopt($ch, CURLOPT_HTTPHEADER, array(
		'Content-Type: application/json',
		'Accept: application/json'));
	curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, false);
	$response=curl_exec($ch);
	$response = json_decode($response,true) ;
	curl_close($ch);

	// echo"<pre>";print_r($response);
}


// From URL to get webpage contents. 
$url = "http://localhost:8080/machine_test/api/state/read.php"; 

// Initialize a CURL session. 
$ch = curl_init();  

// Return Page contents. 
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 

//grab URL and pass it to the variable. 
curl_setopt($ch, CURLOPT_URL, $url); 

$result = curl_exec($ch);
$states = json_decode($result,true) ;
curl_close($ch);
//Result check
// print_r($states[records]); 

?>
<!DOCTYPE html>
<html>
<head>
	<title>User Details</title>
	<!-- CSS only -->
	<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" >


	<!-- JS, Popper.js, and jQuery -->
	<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
	<style type="text/css">
		.form-div { margin-top: 100px; border: 1px solid #e0e0e0; }
		#profileDisplay { display: block; height: 210px; width: 210px; margin: 0px auto; border-radius: 50%; }
		.img-placeholder {
			width: 210px;
			color: white;
			height: 100%;
			background: black;
			opacity: .7;
			height: 210px;
			border-radius: 50%;
			z-index: 2;
			position: absolute;
			left: 50%;
			transform: translateX(-50%);
			display: none;
		}
		.img-placeholder h4 {
			margin-top: 40%;
			color: white;
		}
		.img-div:hover .img-placeholder {
			display: block;
			cursor: pointer;
		}
		input[type="file"] {
			display: block;
		}
		.imageThumb {
			max-height: 75px;
			border: 2px solid;
			padding: 1px;
			cursor: pointer;
		}
		.pip {
			display: inline-block;
			margin: 10px 10px 0 0;
		}
		.remove {
			display: block;
			background: #444;
			border: 1px solid black;
			color: white;
			text-align: center;
			cursor: pointer;
		}
		.remove:hover {
			background: white;
			color: black;
		}
	</style>
</head>
<body>
	<nav class="navbar navbar-expand-lg navbar-light bg-light">
		<a class="navbar-brand" href="#">Demo</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>

		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<ul class="navbar-nav mr-auto">
				<li class="nav-item dropdown">
					<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						User Details
					</a>
					<div class="dropdown-menu" aria-labelledby="navbarDropdown">
						<a class="dropdown-item" href="user_list.php">All details</a>
						<a class="dropdown-item" href="add_new.php">Add New</a>
					</div>
				</li>
			</ul>
		</div>
	</nav>
	<div class="container">
		<div class="row">
			<div class="col-8 mx-auto mt-5 mb-5">
				<?php 
				if(isset($_SESSION['msg'])):?>



					<div class="alert alert-info alert-dismissible fade show" role="alert">

						<?php echo $_SESSION['msg']; ?>

						<button type="button" class="close" data-dismiss="alert" aria-label="Close">

							<span aria-hidden="true">&times;</span>

						</button>

					</div>

				<?php endif; ?>

				<?php unset($_SESSION['msg']); ?>
				<form id="form_data" action="backend.php" method="POST" enctype="multipart/form-data">
					<div class="row">
						<div class="col-12">
							<div class="form-group text-center" style="position: relative;" >
								<span class="img-div" style="width: 150px;height: 150px">
									<div class="text-center img-placeholder"  onClick="triggerClick()">
										<h4>Upload image</h4>
									</div>
									<img src="images/avatar.jpg" onClick="triggerClick()" id="profileDisplay">
								</span>
								<input type="file" name="profileImage" onChange="displayImage(this)" id="profileImage" class="form-control" style="display: none;">
								<label>Profile Image</label>
							</div>
						</div>
					</div>
					<div class="row mt-3">
						<div class="col">
							<?php if (isset($_GET[id])) { ?>
								<input type="hidden" name="uid" value="<?php if(!empty($response[id])): echo $response[id]; endif;?>">
							<?php } ?>
							
							<input type="text" class="form-control" name="name" value="<?php if(!empty($response[name])): echo $response[name]; endif;?>" placeholder="Enter name" required>
						</div>
						<div class="col">
							<input type="date" class="form-control" name="dob" value="<?php if(!empty($response[birthday])): echo $response[birthday]; endif;?>" placeholder="Select Birthday" required>
						</div>
						<div class="col">
							<div class="form-check form-check-inline">
								<input class="form-check-input" type="radio" name="gender" id="male" value="1" <?php if($response['gender']=="1"){ echo "checked";}?>>
								<label class="form-check-label" for="male">Male</label>
							</div>
							<div class="form-check form-check-inline">
								<input class="form-check-input" type="radio" name="gender" id="female" value="2"<?php if($response['gender']=="2"){ echo "checked";}?>>
								<label class="form-check-label" for="female">Female</label>
							</div>
						</div>
					</div>
					<div class="row mt-3">
						<div class="col">
							<select class="custom-select" id="state" name="state" required>
								<option selected>Select State</option>
								<?php foreach ($states[records] as  $value) { ?>
									<option value="<?php echo  $value[id] ?>" <?php if($response['state_name']==$value[name]){ echo "selected";}?>><?php echo   $value[name]; ?></option>
								<?php } ?>
							</select>
						</div>
						<div class="col">
							<select class="custom-select" id="city" name="city" required>
								<option value="<?php echo  $response[city_id] ?>" <?php if(!empty($response['city_id'])){ echo "selected";}?>><?php if(!empty($response['city_id'])){ echo $response['city_name'];}else{?>Select City <?php } ?></option>
							</select>
						</div>
					</div>
					<div class="row mt-3">
						<div class="col-12">
							<div class="field_wrapper">
								<div>
									<?php if (isset($_GET[id])) { ?>


										<?php $edu = explode(", ", $response['education']); ?>
										<?php $passing_year = explode(", ", $response['passing_year']);?>
										<div class="input-group mb-3">
											<?php foreach ($edu as $key=> $value) { ?>
												<div class="input-group-prepend">
													<input type="text" class="form-control" placeholder="Enter education details" name="education[]" value="<?php echo $value; ?>" required />
												</div>
											<?php } ?>
											<?php foreach ($passing_year as $key=> $value) { ?>
												<input type="text" class="form-control"  placeholder="Year Of Completion" name="year_Of_completion[]" value="<?php echo $value; ?>" required />
											<?php } ?>
											<div class="input-group-append">
												<span class="input-group-text">	<a href="javascript:void(0);" class="add_button_edu" title="Add field">ADD</a></span>
											</div>
										</div>
									<?php }else{ ?>
										<div class="input-group mb-3">
											<div class="input-group-prepend">
												<input type="text" class="form-control" placeholder="Enter education details" name="education[]" value="" required />
											</div>
											<input type="text" class="form-control"  placeholder="Year Of Completion" name="year_Of_completion[]" value="" required />
											<div class="input-group-append">
												<span class="input-group-text">	<a href="javascript:void(0);" class="add_button_edu" title="Add field">ADD</a></span>
											</div>
										</div>
									<?php } ?>
								</div>
							</div>
						</div>
					</div>
					<div class="row mt-3">
						<div class="col-12">
							<label for="exampleInputEmail1">Select skill</label>
							<select class="form-control js-example-basic-multiple" name="skills[]" multiple="multiple" required>
								<option value="" disabled>Select skill</option>
								<option value="php" <?php if(!empty($response['skill'])){ echo "selected";}?>>PHP</option>
								<option value="laravel" <?php if(!empty($response['skill'])){ echo "selected";}?>>Laravel</option>
							</select>
						</div>
					</div>
					<div class="row mt-3">
						<div class="col-12">
							<div class="field" align="left">
								<h3>Upload your certificates</h3>
								<input type="file" id="files" name="files[]" multiple required>
							</div>
						</div>
					</div>
					<div class="row mt-3">
						<div class="col-12">
							<div class="form-check form-check-inline">
								<input class="form-check-input" type="radio" name="profession" id="salaried" value="salaried" <?php if($response['profession']=="Salaried"){ echo "checked";}?>>
								<label class="form-check-label" for="salaried">Salaried</label>
							</div>
							<div class="form-check form-check-inline">
								<input class="form-check-input" type="radio" name="profession" id="self_employed" value="self_employed" <?php if($response['profession']=="self_employed"){ echo "checked";}?>>
								<label class="form-check-label" for="self_employed">Self-employed</label>
							</div>
						</div>
					</div>
					<div id="salaried_details" style="display: none;">
						<div class="row mt-3">
							<div class="col">
								<input type="text" class="form-control" value="<?php if(!empty($response[company_name])): echo $response[company_name]; endif;?>" name="cname" placeholder="Company name">
							</div>
							<div class="col">
								<input type="date" class="form-control" value="<?php if(!empty($response[date_of_joining])): echo $response[date_of_joining]; endif;?>" name="doj" placeholder="Date of joining">
							</div>
						</div>	
					</div>
					<div id="self_employed_details" style="display: none;">
						<div class="row mt-3">
							<div class="col">
								<input type="text" class="form-control" value="<?php if(!empty($response[business_name])): echo $response[business_name]; endif;?>" name="bname" placeholder="Business name">
							</div>
							<div class="col">
								<input type="text" class="form-control" value="<?php if(!empty($response[location])): echo $response[location]; endif;?>" name="location" placeholder="Location">
							</div>
						</div>
					</div>

					<div class="row mt-3">
						<div class="col">
							<input type="email" class="form-control" name="email_id" value="<?php if(!empty($response[email_id])): echo $response[email_id]; endif;?>" placeholder="Enter Email" required>
						</div>
						<div class="col">
							<input type="tel" class="form-control" name="mobile_no" value="<?php if(!empty($response[mobile_no])): echo $response[mobile_no]; endif;?>" placeholder="Enter mobile no" required>
						</div>
					</div>
					<div class="row mt-3">
						<div class="col-12">
							<button name="submit" class="btn btn-primary float-right" id="submit_id">Submit</button>
						</div>
					</div>



				</form>
			</div>
		</div>

	</div>

</body>
</html>

<script>  
	$('select#state').on('change', function() {
		var state_id = this.value;


		$.ajax({  
			url: 'http://localhost:8080/machine_test/api/city/find_city.php?id='+state_id,
			type: 'GET',  
			dataType: 'json',  
			success: function(data, textStatus, xhr) { 
				$('#city').empty()
				$.each( data.records, function( i, val ) {
					$('<option>').val(val.id).text(val.name).appendTo('#city');  
					// $( "#" + i ).append( document.createTextNode( " - "  +val ) );
				}); 
				// console.log(data.records);
				// $('<option>').val('999').text('999').appendTo('#city');  
			},  
			error: function(xhr, textStatus, errorThrown) {  
				console.log('Error in Database');  
			}  
		});
	});
	$(document).ready(function() {  
    var maxField = 10; //Input fields increment limitation
    var addButton = $('.add_button_edu'); //Add button selector
    var wrapper = $('.field_wrapper'); //Input field wrapper
    var fieldHTML = '<div>	<div class="input-group mb-3"><div class="input-group-prepend"><input type="text" class="form-control" placeholder="Enter education details" name="education[]" value=""/></div><input type="text" class="form-control"  placeholder="Year Of Completion" name="year_Of_completion[]" value=""/><div class="input-group-append remove_button "><span class="input-group-text">	<a href="javascript:void(0);" class="add_button_edu" title="Add field">Remove</a></span></div></div></div>'; //New input field html 
    var x = 1; //Initial field counter is 1
    
    //Once add button is clicked
    $(addButton).click(function(){
        //Check maximum number of input fields
        if(x < maxField){ 
            x++; //Increment field counter
            $(wrapper).append(fieldHTML); //Add field html
        }
    });
    
    //Once remove button is clicked
    $(wrapper).on('click', '.remove_button', function(e){
    	e.preventDefault();
        $(this).parent('div').remove(); //Remove field html
        x--; //Decrement field counter
    });
}); 
	function triggerClick(e) {
		document.querySelector('#profileImage').click();
	}
	function displayImage(e) {
		if (e.files[0]) {
			var reader = new FileReader();
			reader.onload = function(e){
				document.querySelector('#profileDisplay').setAttribute('src', e.target.result);
			}
			reader.readAsDataURL(e.files[0]);
		}
	}
	$(document).ready(function() {
		$('.js-example-basic-multiple').select2();
	});
	$(document).ready(function() {
		var img = "images/"+"<?php echo $response[photo] ?>";
		// $('#profileImage').val(img);
		if (window.File && window.FileList && window.FileReader) {
			$("#files").on("change", function(e) {
				var files = e.target.files,
				filesLength = files.length;
				for (var i = 0; i < filesLength; i++) {
					var f = files[i]
					var fileReader = new FileReader();
					fileReader.onload = (function(e) {
						var file = e.target;
						$("<span class=\"pip\">" +
							"<img class=\"imageThumb\" src=\"" + e.target.result + "\" title=\"" + file.name + "\"/>" +
							"<br/><span class=\"remove\">Remove image</span>" +
							"</span>").insertAfter("#files");
						$(".remove").click(function(){
							$(this).parent(".pip").remove();
						});

          // Old code here
          /*$("<img></img>", {
            class: "imageThumb",
            src: e.target.result,
            title: file.name + " | Click to remove"
        }).insertAfter("#files").click(function(){$(this).remove();});*/

    });
					fileReader.readAsDataURL(f);
				}
			});
		} else {
			alert("Your browser doesn't support to File API")
		}
	});

	if($('#salaried').prop("checked") == true){
		$('#salaried_details').css('display','block');
		$('#self_employed_details').css('display','none');
	}
	if($('#self_employed').prop("checked") == true){
		$('#salaried_details').css('display','none');
		$('#self_employed_details').css('display','block');
	}
	$('#salaried').click(function(){
		$('#salaried_details').css('display','block');
		$('#self_employed_details').css('display','none');

	});
	$('#self_employed').click(function(){
		$('#salaried_details').css('display','none');
		$('#self_employed_details').css('display','block');

	});
	$('#submit_id').click(function(){
		$('#form_data').submit();

	});

</script>
