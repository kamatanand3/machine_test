<?php 
session_start();
// From URL to get webpage contents. 
$url = "http://localhost:8080/machine_test/api/details/read.php"; 

// Initialize a CURL session. 
$ch = curl_init();  

// Return Page contents. 
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 

//grab URL and pass it to the variable. 
curl_setopt($ch, CURLOPT_URL, $url); 

$result = curl_exec($ch);
$all_data = json_decode($result,true) ;
curl_close($ch);
//Result check
// print_r($all_data[records]); 

?>
<!DOCTYPE html>
<html>
<head>
	<title>User Details</title>
	<!-- CSS only -->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" >

	<!-- JS, Popper.js, and jQuery -->
	<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
</head>
<body>
	<nav class="navbar navbar-expand-lg navbar-light bg-light">
		<a class="navbar-brand" href="#">Demo</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>

		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<ul class="navbar-nav mr-auto">
				<li class="nav-item dropdown">
					<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						User Details
					</a>
					<div class="dropdown-menu" aria-labelledby="navbarDropdown">
						<a class="dropdown-item" href="user_list.php">All details</a>
						<a class="dropdown-item" href="add_new.php">Add New</a>
					</div>
				</li>
			</ul>
		</div>
	</nav>
	<div class="container">
		<div class="row">
			<div class="col-8 mx-auto mt-5">
				<?php 
				if(isset($_SESSION['msg'])):?>



					<div class="alert alert-info alert-dismissible fade show" role="alert">

						<?php echo $_SESSION['msg']; ?>

						<button type="button" class="close" data-dismiss="alert" aria-label="Close">

							<span aria-hidden="true">&times;</span>

						</button>

					</div>

				<?php endif; ?>

				<?php unset($_SESSION['msg']); ?>
				<table class="table table-striped table-dark">
					<thead>
						<tr>
							<th scope="col">Name</th>
							<th scope="col">Email</th>
							<th scope="col">Mobile</th>
							<th scope="col">Action</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($all_data[records] as  $value) { ?>
							<tr>
								<th><?php echo $value[name] ?></th>
								<td><?php echo $value[email_id] ?></td>
								<td><?php echo $value[mobile_no] ?></td>
								<td><div class="btn-group" role="group" aria-label="Basic example">
									<a href="add_new.php?id=<?php echo $value[id] ?>"><button type="button" class="btn btn-sm btn-info">EDIT</button></a>
									<a href="backend.php?id=<?php echo $value[id] ?>"><button type="button" class="btn btn-sm btn-danger">DELETE</button></a>
								</div></td>

							</tr>
						<?php } ?>
						
					</tbody>
				</table>
			</div>
		</div>
		
	</div>

</body>
</html>