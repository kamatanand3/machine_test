-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 16, 2020 at 08:15 AM
-- Server version: 10.4.10-MariaDB
-- PHP Version: 7.2.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `machine_test`
--

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE `city` (
  `id` int(11) NOT NULL,
  `city_name` varchar(255) NOT NULL,
  `state_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `city`
--

INSERT INTO `city` (`id`, `city_name`, `state_id`, `created_at`) VALUES
(1, 'Mumbai', 1, '2020-06-13 08:00:23'),
(2, 'Pune', 1, '2020-06-13 08:00:23'),
(3, 'Darbhanga', 2, '2020-06-13 08:00:54'),
(4, 'Madhubani', 2, '2020-06-13 08:00:54');

-- --------------------------------------------------------

--
-- Table structure for table `state`
--

CREATE TABLE `state` (
  `id` int(11) NOT NULL,
  `state_name` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `state`
--

INSERT INTO `state` (`id`, `state_name`, `created_at`) VALUES
(1, 'Maharashtra', '2020-06-13 07:59:38'),
(2, 'Bihar', '2020-06-13 07:59:38');

-- --------------------------------------------------------

--
-- Table structure for table `user_details`
--

CREATE TABLE `user_details` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `birthday` date NOT NULL,
  `gender` int(11) NOT NULL,
  `state` int(11) NOT NULL,
  `city` int(11) NOT NULL,
  `education` varchar(255) NOT NULL,
  `passing_year` varchar(255) NOT NULL,
  `photo` varchar(255) NOT NULL,
  `skill` longtext NOT NULL,
  `certificates` longtext NOT NULL,
  `profession` varchar(255) NOT NULL,
  `company_name` varchar(255) DEFAULT NULL,
  `date_of_joining` date DEFAULT NULL,
  `business_name` varchar(255) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `email_id` varchar(255) NOT NULL,
  `mobile_no` int(15) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user_details`
--

INSERT INTO `user_details` (`id`, `name`, `birthday`, `gender`, `state`, `city`, `education`, `passing_year`, `photo`, `skill`, `certificates`, `profession`, `company_name`, `date_of_joining`, `business_name`, `location`, `email_id`, `mobile_no`, `created_at`) VALUES
(1, 'Anand1', '1996-02-09', 1, 2, 1, 'BSC', '2018', '1592286684-', 'php, laravel', '7ef83139-4d63-4514-b3e2-71c579808f3f (2).jpg', 'salaried', 'FH', '2020-02-03', '', '', 'kamatanand3@gmail.com', 2147483647, '2020-06-16 02:21:24'),
(3, 'marks', '2020-06-17', 1, 1, 0, 'asdasd, asdasd`', '2018, 2019', '1592252853-mark.png', 'php, laravel', 'addhhar.png, back_addar.png', 'salaried', 'test', '2020-06-26', '', '', 'kamatanand3@gmail.com', 2147483647, '2020-06-15 16:57:34'),
(4, 'marks', '2020-06-17', 1, 1, 0, 'asdasd, asdasd`', '2018, 2019', '1592253163-mark.png', 'php, laravel', 'addhhar.png, back_addar.png', 'salaried', 'test', '2020-06-26', '', '', 'kamatanand3@gmail.com', 2147483647, '2020-06-15 17:02:43'),
(5, 'neha', '1997-08-13', 2, 1, 1, 'ssc, hsc', '2013, 2015', '1592253465-ne.png', 'php, laravel', 'mark.png, WhatsApp Image 2019-04-21 at 6.48.03 PM.jpeg', 'self_employed', '', '0000-00-00', 'test', 'mumbai', 'nehatripathi202018@gmail.com', 740033144, '2020-06-15 17:07:45');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `city`
--
ALTER TABLE `city`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `state`
--
ALTER TABLE `state`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_details`
--
ALTER TABLE `user_details`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `city`
--
ALTER TABLE `city`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `state`
--
ALTER TABLE `state`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `user_details`
--
ALTER TABLE `user_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
