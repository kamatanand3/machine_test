<?php
class Details{

    // database connection and table name
    private $conn;
    private $table_name = "user_details";

    // object properties
    public $id;
    public $name;
    public $birthday;
    public $gender;
    public $state;
    public $city;
    public $education;
    public $passing_year;
    public $photo;
    public $skill;
    public $certificates;
    public $profession;
    public $company_name;
    public $date_of_joining;
    public $business_name;
    public $location;
    public $email_id;
    public $mobile_no;
    public $created_at;

    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
    }



    // read user_details
    function read(){

    // select all query
        $query = "SELECT u.*,c.city_name as city_name,s.state_name as state_name FROM  " . $this->table_name . " u
        LEFT JOIN state s ON s.id = u.state
        LEFT JOIN city c ON c.state_id = s.id
        GROUP BY u.id";

    // prepare query statement
        $stmt = $this->conn->prepare($query);

    // execute query
        $stmt->execute();

        return $stmt;
    }


// create product
    function create(){

    // query to insert record
        $query = "INSERT INTO
        " . $this->table_name . "
        SET
        name=:name, birthday=:birthday, gender=:gender, state=:state, city=:city, education=:education, passing_year=:passing_year, photo=:photo, skill=:skill, certificates=:certificates, profession=:profession, company_name=:company_name, date_of_joining=:date_of_joining, business_name=:business_name, location=:location, email_id=:email_id, mobile_no=:mobile_no, created_at=:created_at";

    // prepare query
        $stmt = $this->conn->prepare($query);

    // sanitize
        $this->name=htmlspecialchars(strip_tags($this->name));
        $this->birthday=htmlspecialchars(strip_tags($this->birthday));
        $this->gender=htmlspecialchars(strip_tags($this->gender));
        $this->state=htmlspecialchars(strip_tags($this->state));
        $this->city=htmlspecialchars(strip_tags($this->city));
        $this->education=htmlspecialchars(strip_tags($this->education));
        $this->passing_year=htmlspecialchars(strip_tags($this->passing_year));
        $this->photo=htmlspecialchars(strip_tags($this->photo));
        $this->skill=htmlspecialchars(strip_tags($this->skill));
        $this->certificates=htmlspecialchars(strip_tags($this->certificates));
        $this->profession=htmlspecialchars(strip_tags($this->profession));
        $this->company_name=htmlspecialchars(strip_tags($this->company_name));
        $this->date_of_joining=htmlspecialchars(strip_tags($this->date_of_joining));
        $this->business_name=htmlspecialchars(strip_tags($this->business_name));
        $this->location=htmlspecialchars(strip_tags($this->location));
        $this->email_id=htmlspecialchars(strip_tags($this->email_id));
        $this->mobile_no=htmlspecialchars(strip_tags($this->mobile_no));
        $this->created_at=htmlspecialchars(strip_tags($this->created_at));

    // bind values
        $stmt->bindParam(":name", $this->name);
        $stmt->bindParam(":birthday", $this->birthday);
        $stmt->bindParam(":gender", $this->gender);
        $stmt->bindParam(":state", $this->state);
        $stmt->bindParam(":city", $this->city);
        $stmt->bindParam(":education", $this->education);
        $stmt->bindParam(":passing_year", $this->passing_year);
        $stmt->bindParam(":photo", $this->photo);
        $stmt->bindParam(":skill", $this->skill);
        $stmt->bindParam(":certificates", $this->certificates);
        $stmt->bindParam(":profession", $this->profession);
        $stmt->bindParam(":company_name", $this->company_name);
        $stmt->bindParam(":date_of_joining", $this->date_of_joining);
        $stmt->bindParam(":business_name", $this->business_name);
        $stmt->bindParam(":location", $this->location);
        $stmt->bindParam(":email_id", $this->email_id);
        $stmt->bindParam(":mobile_no", $this->mobile_no);
        $stmt->bindParam(":created_at", $this->created_at);

    // execute query
        if($stmt->execute()){
            return true;
        }

        return false;

    }

    // used when filling up the update product form
    function readOne(){

    // query to read single record
        $query = "SELECT u.*,c.city_name as city_name,s.state_name as state_name FROM  " . $this->table_name . " u
        LEFT JOIN state s ON s.id = u.state
        LEFT JOIN city c ON c.state_id = s.id
        WHERE
        u.id = ?
        LIMIT
        0,1";

    // prepare query statement
        $stmt = $this->conn->prepare( $query );

    // bind id of product to be updated
        $stmt->bindParam(1, $this->id);

    // execute query
        $stmt->execute();

    // get retrieved row
        $row = $stmt->fetch(PDO::FETCH_ASSOC);

    // set values to object properties
        $this->name = $row['name'];
        $this->birthday = $row['birthday'];
        $this->gender = $row['gender'];
        $this->state_name = $row['state_name'];
        $this->city_name = $row['city_name'];
        $this->city_id = $row['city'];
        $this->education = $row['education'];
        $this->passing_year = $row['passing_year'];
        $this->photo = $row['photo'];
        $this->skill = $row['skill'];
        $this->certificates = $row['certificates'];
        $this->profession = $row['profession'];
        $this->company_name = $row['company_name'];
        $this->date_of_joining = $row['date_of_joining'];
        $this->business_name = $row['business_name'];
        $this->location = $row['location'];
        $this->email_id = $row['email_id'];
        $this->mobile_no = $row['mobile_no'];
        $this->created_at = $row['created_at'];
    }

    // update the product
    function update(){

    // update query
        $query = "UPDATE
        " . $this->table_name . "
        SET
        name=:name,
        birthday=:birthday,
        gender=:gender, 
        state=:state,
        city=:city, 
        education=:education,
        passing_year=:passing_year,
        photo=:photo, skill=:skill, 
        certificates=:certificates,
        profession=:profession, 
        company_name=:company_name,
        date_of_joining=:date_of_joining,
        business_name=:business_name, 
        location=:location, 
        email_id=:email_id,
        mobile_no=:mobile_no,
        created_at=:created_at
        WHERE
        id = :id";

    // prepare query statement
        $stmt = $this->conn->prepare($query);

    // sanitize
        $this->name=htmlspecialchars(strip_tags($this->name));
        $this->birthday=htmlspecialchars(strip_tags($this->birthday));
        $this->gender=htmlspecialchars(strip_tags($this->gender));
        $this->state=htmlspecialchars(strip_tags($this->state));
        $this->city=htmlspecialchars(strip_tags($this->city));
        $this->education=htmlspecialchars(strip_tags($this->education));
        $this->passing_year=htmlspecialchars(strip_tags($this->passing_year));
        $this->photo=htmlspecialchars(strip_tags($this->photo));
        $this->skill=htmlspecialchars(strip_tags($this->skill));
        $this->certificates=htmlspecialchars(strip_tags($this->certificates));
        $this->profession=htmlspecialchars(strip_tags($this->profession));
        $this->company_name=htmlspecialchars(strip_tags($this->company_name));
        $this->date_of_joining=htmlspecialchars(strip_tags($this->date_of_joining));
        $this->business_name=htmlspecialchars(strip_tags($this->business_name));
        $this->location=htmlspecialchars(strip_tags($this->location));
        $this->email_id=htmlspecialchars(strip_tags($this->email_id));
        $this->mobile_no=htmlspecialchars(strip_tags($this->mobile_no));
        $this->created_at=htmlspecialchars(strip_tags($this->created_at));
        $this->id=htmlspecialchars(strip_tags($this->id));

    // bind new values
        $stmt->bindParam(":name", $this->name);
        $stmt->bindParam(":birthday", $this->birthday);
        $stmt->bindParam(":gender", $this->gender);
        $stmt->bindParam(":state", $this->state);
        $stmt->bindParam(":city", $this->city);
        $stmt->bindParam(":education", $this->education);
        $stmt->bindParam(":passing_year", $this->passing_year);
        $stmt->bindParam(":photo", $this->photo);
        $stmt->bindParam(":skill", $this->skill);
        $stmt->bindParam(":certificates", $this->certificates);
        $stmt->bindParam(":profession", $this->profession);
        $stmt->bindParam(":company_name", $this->company_name);
        $stmt->bindParam(":date_of_joining", $this->date_of_joining);
        $stmt->bindParam(":business_name", $this->business_name);
        $stmt->bindParam(":location", $this->location);
        $stmt->bindParam(":email_id", $this->email_id);
        $stmt->bindParam(":mobile_no", $this->mobile_no);
        $stmt->bindParam(":created_at", $this->created_at);
        $stmt->bindParam(':id', $this->id);

    // execute the query
        if($stmt->execute()){
            return true;
        }

        return false;
    }
    // delete the product
    function delete(){

    // delete query
        $query = "DELETE FROM " . $this->table_name . " WHERE id = ?";

    // prepare query
        $stmt = $this->conn->prepare($query);

    // sanitize
        $this->id=htmlspecialchars(strip_tags($this->id));

    // bind id of record to delete
        $stmt->bindParam(1, $this->id);

    // execute query
        if($stmt->execute()){
            return true;
        }

        return false;
    }

    // search user_details
    function search($keywords){

    // select all query
        $query = "SELECT
        c.name as category_name, p.id, p.name, p.description, p.price, p.category_id, p.created
        FROM
        " . $this->table_name . " p
        LEFT JOIN
        categories c
        ON p.category_id = c.id
        WHERE
        p.name LIKE ? OR p.description LIKE ? OR c.name LIKE ?
        ORDER BY
        p.created DESC";

    // prepare query statement
        $stmt = $this->conn->prepare($query);

    // sanitize
        $keywords=htmlspecialchars(strip_tags($keywords));
        $keywords = "%{$keywords}%";

    // bind
        $stmt->bindParam(1, $keywords);
        $stmt->bindParam(2, $keywords);
        $stmt->bindParam(3, $keywords);

    // execute query
        $stmt->execute();

        return $stmt;
    }
    // read user_details with pagination
    public function readPaging($from_record_num, $records_per_page){

    // select query
        $query = "SELECT
        c.name as category_name, p.id, p.name, p.description, p.price, p.category_id, p.created
        FROM
        " . $this->table_name . " p
        LEFT JOIN
        categories c
        ON p.category_id = c.id
        ORDER BY p.created DESC
        LIMIT ?, ?";

    // prepare query statement
        $stmt = $this->conn->prepare( $query );

    // bind variable values
        $stmt->bindParam(1, $from_record_num, PDO::PARAM_INT);
        $stmt->bindParam(2, $records_per_page, PDO::PARAM_INT);

    // execute query
        $stmt->execute();

    // return values from database
        return $stmt;
    }
    // used for paging user_details
    public function count(){
        $query = "SELECT COUNT(*) as total_rows FROM " . $this->table_name . "";
        
        $stmt = $this->conn->prepare( $query );
        $stmt->execute();
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        
        return $row['total_rows'];
    }
}
?>  