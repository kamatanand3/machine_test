<?php
class City{

    // database connection and table name
    private $conn;
    private $table_name = "city";

    // object properties
    public $id;


    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
    }





    // read state
    function read(){

    // select all query
        $query = "SELECT * FROM  " . $this->table_name . " c WHERE
        c.state_id = ?";

    // prepare query statement
        $stmt = $this->conn->prepare($query);

         // bind id of product to be updated
        $stmt->bindParam(1, $this->id);

    // execute query
        $stmt->execute();

        return $stmt;
    }

       // read state
    function read_city(){

    // select all query
        $query = "SELECT * FROM  " . $this->table_name . " s  ORDER BY `s`.`city_name` ASC";

    // prepare query statement
        $stmt = $this->conn->prepare($query);

    // execute query
        $stmt->execute();

        return $stmt;
    }







}
?>  