<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Allow-Credentials: true");
header('Content-Type: application/json');
  
// include database and object files
include_once '../config/database.php';
include_once '../objects/details.php';
  
// get database connection
$database = new Database();
$db = $database->getConnection();
  
// prepare details object
$details = new Details($db);
  
// set ID property of record to read
$details->id = isset($_GET['id']) ? $_GET['id'] : die();
  
// read the details of details to be edited
$details->readOne();
  
if($details->name!=null){
    // create array
    $details_arr = array(
        "id" =>  $details->id,
        "name" => $details->name,
        "birthday" => $details->birthday,
        "gender" => $details->gender,
        "state_name" => $details->state_name,
        "city_name" => $details->city_name,
        "city_id" => $details->city_id,
        "education" => $details->education,
        "passing_year" => $details->passing_year,
        "photo" => $details->photo,
        "skill" => $details->skill,
        "certificates" => $details->certificates,
        "profession" => $details->profession,
        "company_name" => $details->company_name,
        "date_of_joining" => $details->date_of_joining,
        "business_name" => $details->business_name,
        "location" => $details->location,
        "email_id" => $details->email_id,
        "mobile_no" => $details->mobile_no,
        "created_at" => $details->created_at
  
    );
  
    // set response code - 200 OK
    http_response_code(200);
  
    // make it json format
    echo json_encode($details_arr);
}
  
else{
    // set response code - 404 Not found
    http_response_code(404);
  
    // tell the user details does not exist
    echo json_encode(array("message" => "Details does not exist."));
}
?>