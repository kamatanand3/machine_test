<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

// include database and object files
include_once '../config/database.php';
include_once '../objects/details.php';

// get database connection
$database = new Database();
$db = $database->getConnection();

// prepare details object
$details = new Details($db);

// get id of details to be edited
$data = json_decode(file_get_contents("php://input"));

// set ID property of details to be edited
$details->id = $data->id;

// set details property values
$details->name = $data->name;
$details->birthday = $data->birthday;
$details->gender = $data->gender;
$details->state = $data->state;
$details->city = $data->city;
$details->education = $data->education;
$details->passing_year = $data->passing_year;
$details->photo = $data->photo;
$details->skill = $data->skill;
$details->certificates = $data->certificates;
$details->profession = $data->profession;
$details->company_name = $data->company_name;
$details->date_of_joining = $data->date_of_joining;
$details->business_name = $data->business_name;
$details->location = $data->location;
$details->email_id = $data->email_id;
$details->mobile_no = $data->mobile_no;
$details->created_at =  date('Y-m-d H:i:s');

// update the details
if($details->update()){
	
    // set response code - 200 ok
	http_response_code(200);
	
    // tell the user
	echo json_encode(array("message" => "Details was updated."));
}

// if unable to update the details, tell the user
else{
	
    // set response code - 503 service unavailable
	http_response_code(503);
	
    // tell the user
	echo json_encode(array("message" => "Unable to update details."));
}
?>