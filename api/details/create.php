<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
  
// get database connection
include_once '../config/database.php';
  
// instantiate details object
include_once '../objects/details.php';
  
$database = new Database();
$db = $database->getConnection();
  
$details = new Details($db);
  
// get posted data
$data = json_decode(file_get_contents("php://input"));
  
// make sure data is not empty
if(
    !empty($data->name) &&
    !empty($data->email_id) &&
    !empty($data->mobile_no)
){
  
    // set details property values
    $details->name = $data->name;
    $details->birthday = $data->birthday;
    $details->gender = $data->gender;
    $details->state = $data->state;
    $details->city = $data->city;
    $details->education = $data->education;
    $details->passing_year = $data->passing_year;
    $details->photo = $data->photo;
    $details->skill = $data->skill;
    $details->certificates = $data->certificates;
    $details->profession = $data->profession;
    $details->company_name = $data->company_name;
    $details->date_of_joining = $data->date_of_joining;
    $details->business_name = $data->business_name;
    $details->location = $data->location;
    $details->email_id = $data->email_id;
    $details->mobile_no = $data->mobile_no;
    $details->created_at =  date('Y-m-d H:i:s');
  
    // create the details
    if($details->create()){
  
        // set response code - 201 created
        http_response_code(201);
  
        // tell the user
        echo json_encode(array("message" => "details was created."));
    }
  
    // if unable to create the details, tell the user
    else{
  
        // set response code - 503 service unavailable
        http_response_code(503);
  
        // tell the user
        echo json_encode(array("message" => "Unable to create details."));
    }
}
  
// tell the user data is incomplete
else{
  
    // set response code - 400 bad request
    http_response_code(400);
  
    // tell the user
    echo json_encode(array("message" => "Unable to create details. Data is incomplete."));
}
?>