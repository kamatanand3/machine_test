<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
  
// database connection will be here

// include database and object files
include_once '../config/database.php';
include_once '../objects/details.php';
  
// instantiate database and details object
$database = new Database();
$db = $database->getConnection();
  
// initialize object
$details = new Details($db);
  
// read detailss will be here
// query detailss
$stmt = $details->read();
$num = $stmt->rowCount();
  
// check if more than 0 record found
if($num>0){
  
    // detailss array
    $detailss_arr=array();
    $detailss_arr["records"]=array();
  
    // retrieve our table contents
    // fetch() is faster than fetchAll()
    // http://stackoverflow.com/questions/2770630/pdofetchall-vs-pdofetch-in-a-loop
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        // extract row
        // this will make $row['name'] to
        // just $name only
        extract($row);
  
        $details_item=array(
            "id" => $id,
            "name" => $name,
            "birthday" => $birthday,
            "gender" => $gender,
            "state" => $state_name,
            "city" => $city_name,
            "city_id" => $city,
            "education" => $education,
            "passing_year" => $passing_year,
            "photo" => $photo,
            "skill" => $skill,
            "certificates" => $certificates,
            "profession" => $profession,
            "company_name" => $company_name,
            "date_of_joining" => $date_of_joining,
            "business_name" => $business_name,
            "location" => $location,
            "email_id" => $email_id,
            "mobile_no" => $mobile_no,
            "created_at" => $created_at
        );
  
        array_push($detailss_arr["records"], $details_item);
    }
  
    // set response code - 200 OK
    http_response_code(200);
  
    // show detailss data in json format
    echo json_encode($detailss_arr);
}else{
  
    // set response code - 404 Not found
    http_response_code(404);
  
    // tell the user no detailss found
    echo json_encode(
        array("message" => "No detailss found.")
    );
}
  
// no detailss found will be here