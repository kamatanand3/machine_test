<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
  
// database connection will be here

// include database and object files
include_once '../config/database.php';
include_once '../objects/city.php';
  
// instantiate database and state object
$database = new Database();
$db = $database->getConnection();
  
// initialize object
$state = new City($db);
  
// read states will be here
// query states
$stmt = $state->read_city();
$num = $stmt->rowCount();
  
// check if more than 0 record found
if($num>0){
  
    // states array
    $states_arr=array();
    $states_arr["records"]=array();
  
    // retrieve our table contents
    // fetch() is faster than fetchAll()
    // http://stackoverflow.com/questions/2770630/pdofetchall-vs-pdofetch-in-a-loop
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        // extract row
        // this will make $row['name'] to
        // just $name only
        extract($row);
  
        $state_item=array(
            "id" => $id,
            "name" => $city_name,
            "created_at" => $created_at
        );
  
        array_push($states_arr["records"], $state_item);
    }
  
    // set response code - 200 OK
    http_response_code(200);
  
    // show states data in json format
    echo json_encode($states_arr);
}else{
  
    // set response code - 404 Not found
    http_response_code(404);
  
    // tell the user no states found
    echo json_encode(
        array("message" => "No states found.")
    );
}
  
// no states found will be here