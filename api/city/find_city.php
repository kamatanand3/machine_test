<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
  
// database connection will be here

// include database and object files
include_once '../config/database.php';
include_once '../objects/city.php';
  
// instantiate database and city object
$database = new Database();
$db = $database->getConnection();
  
// initialize object
$city = new City($db);
// set ID property of record to read
$city->id = isset($_GET['id']) ? $_GET['id'] : die();
  
// read citys will be here
// query citys
$stmt = $city->read();
$num = $stmt->rowCount();
  
// check if more than 0 record found
if($num>0){
  
    // citys array
    $citys_arr=array();
    $citys_arr["records"]=array();
  
    // retrieve our table contents
    // fetch() is faster than fetchAll()
    // http://stackoverflow.com/questions/2770630/pdofetchall-vs-pdofetch-in-a-loop
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        // extract row
        // this will make $row['name'] to
        // just $name only
        extract($row);
  
        $city_item=array(
            "id" => $id,
            "name" => $city_name,
            "created_at" => $created_at
        );
  
        array_push($citys_arr["records"], $city_item);
    }
  
    // set response code - 200 OK
    http_response_code(200);
  
    // show citys data in json format
    echo json_encode($citys_arr);
}else{
  
    // set response code - 404 Not found
    http_response_code(404);
  
    // tell the user no citys found
    echo json_encode(
        array("message" => "No citys found.")
    );
}
  
// no citys found will be here